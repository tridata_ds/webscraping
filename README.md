# Web-scraping-opgaven

## Opgave 1

1. Kijk naar het lego.R script.
2. Wat doet het script? 
3. Zoek op de imdb website de laatste starwars film
4. Kopieer het "lego.R"" script naar "starwars.R"
5. Pas het script aan zodat het de starwars pagina ophaalt
6. Wat is de rating?
7. Voeg aan het script een stuk toe waarmee de story line wordt opgehaald
(id is "#titleStoryLine")
8. Voeg het script aan git toe 
9. Commit het script.
10. Vind de release date en haal die op met `rvest`.

## Opgave 2

- Kopieer het lego.R script naar benzine_prijzen.R
- Ga naar de website "benzine-prijzen.info"
- Pas het script aan zodat je de adviesprijzen kunt ophalen ("body > center > table:nth-child(12)")
- Pas het script aan zodat je dat courante prijzen voor "Den Haag" kunt ophalen.

## Opgave 3

- Maak een markdown rapport met RStudio, waarin je de courante prijzen uit je gemeente 
automatisch ophaalt en in tabel vorm weergeeft. 
- gebruik hiervoor de `kable` functie die in de bibliotheek `knitr` zit.


## Opgave 4

- Open de website gaspedaal.nl
- We gaan op zoek naar een zwarte polo, die niet meer dan 5000 euro mag kosten, op benzine rijdt en handgeschakeld is. 
- Hoeveel kun je er vinden?
- Haal deze pagina op met `rvest` en maak een query m.b.v. van `html_nodes` om alle items waarin een zoekresultaat staat te selecten (zit in een <li></li> element)
- Schrijf code die de link (url), title en kmstand uit de resultaten haalt en sla die op een in data.frame "polo.csv", gebruik hiervoor eventueel stringr functies om de code op te schonen.
- Klik op de pagina op "volgende" om de volgende zoekresultaten te vinden. Welke url (hyperlink) staat er nu in de adresbalk? Gebruik dit feit om  een loop te schrijven die alle resultpagina's afloopt, scrapet en de resultaten opslaat in een databestand.
- Maak een histogram van de km-standen
- Maak een plot van de prijs vs km-stand.

## Opgave 5

- Zoek "James Bond" films in IMDB en haal de titels, jaartal en rating op.
- Sla dit op in een data.frame.

- Sla voor "James Bond" films the afzonderlijke urls naar film pagina op.
- gebruik hiervoor `html_attr` href.

- loop over de urls en haal voor iedere film de release date op.
